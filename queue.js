let collection = [];
// Write the queue functions below.
// Print (output all the elements of the queue)
function print() {

    return collection;
};

// Enqueue (add element to rear of queue)
function enqueue(element) {

    collection[collection.length] = element;

    return collection;
};

// Dequeue (remove element at front of queue)
function dequeue() {
    let addCollection = [];

    for (let i = 1; i<collection.length; i++){addCollection[i-1] = collection[i];}
        collection = addCollection;

    return collection;
};

// Front (show element at the front)
function front() {

    return collection [0];
};

// Size (show total number of elements)
function size() {

    return collection.length;
};

// isEmpty (outputs Boolean value describing whether queue is empty or not)
function isEmpty() {

    return collection.length === 0;
};


module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};